#include <Audio.h>
#include <Wire.h>
#include <SPI.h>
#include <SD.h>
#include <SerialFlash.h>

AudioInputUSB       usb1;
AudioOutputUSB      usb2;
AudioInputAnalog	adc1;
AudioOutputAnalog	dac1;

AudioMixer4			mixer1;
AudioMixer4			mixer2;

AudioConnection     patchCord1(usb1, 0, mixer1, 0);
AudioConnection		patchChord2(usb1, 1, mixer2, 0);
AudioConnection     patchChord3(mixer1, 0, usb2, 0);
AudioConnection     patchChord4(mixer2, 0, usb2, 1);

AudioConnection	    patchChord5(adc1, 0, dac1, 0);


void setup() {
	Serial.begin(115200);
	Serial.print("USB Passtrough with gain v1.0\n");
	AudioMemory(20);
	mixer1.gain(0, 0.5);
	mixer2.gain(0, 0.5);
}

void loop() {

  delay(100);
}

